<?php
setlocale(LC_ALL,"es_ES");
define('SITE_ROOT', dirname(__FILE__));
header('Content-Type: text/html; charset=utf-8');
ini_set('display_errors', 'On');
error_reporting(E_ALL);

// Ruta de los ficheros de funciones PHP que cargamos
include './simple_html_dom.php';

//'1956 – Lugano' => 'http://www.eurovision.tv/page/history/by-year/contest?event=273#Scoreboard', -> no tira!

$urls = [
'1957 – Frankfurt' => 'http://www.eurovision.tv/page/history/by-year/contest?event=274#Scoreboard',
'1958 – Hilversum' => 'http://www.eurovision.tv/page/history/by-year/contest?event=275#Scoreboard',
'1959 – Cannes' => 'http://www.eurovision.tv/page/history/by-year/contest?event=276#Scoreboard',
'1960 – London' => 'http://www.eurovision.tv/page/history/by-year/contest?event=277#Scoreboard',
'1961 – Cannes' => 'http://www.eurovision.tv/page/history/by-year/contest?event=278#Scoreboard',
'1962 – Luxemburg' => 'http://www.eurovision.tv/page/history/by-year/contest?event=279#Scoreboard',
'1963 – London' => 'http://www.eurovision.tv/page/history/by-year/contest?event=280#Scoreboard',
'1964 – Copenhagen' => 'http://www.eurovision.tv/page/history/by-year/contest?event=281#Scoreboard',
'1965 – Nápoles' => 'http://www.eurovision.tv/page/history/by-year/contest?event=282#Scoreboard',
'1966 – Luxemburgo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=283#Scoreboard',
'1967 – Viena' => 'http://www.eurovision.tv/page/history/by-year/contest?event=284#Scoreboard',
'1968 – Londres' => 'http://www.eurovision.tv/page/history/by-year/contest?event=285#Scoreboard',
'1969 – Madrid' => 'http://www.eurovision.tv/page/history/by-year/contest?event=286#Scoreboard',
'1970 – Amsterdam' => 'http://www.eurovision.tv/page/history/by-year/contest?event=223#Scoreboard',
'1971 – Dublín' => 'http://www.eurovision.tv/page/history/by-year/contest?event=287#Scoreboard',
'1972 – Edimburgo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=288#Scoreboard',
'1973 – Luxemburgo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=289#Scoreboard',
'1974 – Brighton' => 'http://www.eurovision.tv/page/history/by-year/contest?event=290#Scoreboard',
'1975 – Estocolmo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=291#Scoreboard',
'1976 – La Haya' => 'http://www.eurovision.tv/page/history/by-year/contest?event=292#Scoreboard',
'1977 – Londres' => 'http://www.eurovision.tv/page/history/by-year/contest?event=293#Scoreboard',
'1978 – París' => 'http://www.eurovision.tv/page/history/by-year/contest?event=294#Scoreboard',
'1979 – Jerusalem' => 'http://www.eurovision.tv/page/history/by-year/contest?event=295#Scoreboard',
'1980 – La Haya' => 'http://www.eurovision.tv/page/history/by-year/contest?event=296#Scoreboard',
'1981 – Dublín' => 'http://www.eurovision.tv/page/history/by-year/contest?event=297#Scoreboard',
'1982 – Harrogate' => 'http://www.eurovision.tv/page/history/by-year/contest?event=298#Scoreboard',
'1983 – Munich' => 'http://www.eurovision.tv/page/history/by-year/contest?event=299#Scoreboard',
'1984 – Luxemburgo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=300#Scoreboard',
'1985 – Gotheborg' => 'http://www.eurovision.tv/page/history/by-year/contest?event=301#Scoreboard',
'1986 – Bergen' => 'http://www.eurovision.tv/page/history/by-year/contest?event=302#Scoreboard',
'1987 – Bruselas' => 'http://www.eurovision.tv/page/history/by-year/contest?event=303#Scoreboard',
'1988 – Dublín' => 'http://www.eurovision.tv/page/history/by-year/contest?event=304#Scoreboard',
'1989 – Lausanne' => 'http://www.eurovision.tv/page/history/by-year/contest?event=305#Scoreboard',
'1990 – Zagreb' => 'http://www.eurovision.tv/page/history/by-year/contest?event=306#Scoreboard',
'1991 – Roma' => 'http://www.eurovision.tv/page/history/by-year/contest?event=307#Scoreboard',
'1992 – Malmö' => 'http://www.eurovision.tv/page/history/by-year/contest?event=308#Scoreboard',
'1993 – Millstreet' => 'http://www.eurovision.tv/page/history/by-year/contest?event=235#Scoreboard',
'1994 – Dublín' => 'http://www.eurovision.tv/page/history/by-year/contest?event=309#Scoreboard',
'1995 – Dublín' => 'http://www.eurovision.tv/page/history/by-year/contest?event=310#Scoreboard',
'1996 – Oslo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=311#Scoreboard',
'1997 – Dublín' => 'http://www.eurovision.tv/page/history/by-year/contest?event=312#Scoreboard',
'1998 – Birmingham' => 'http://www.eurovision.tv/page/history/by-year/contest?event=313#Scoreboard',
'1999 – Jerusalem' => 'http://www.eurovision.tv/page/history/by-year/contest?event=314#Scoreboard',
'2000 – Estocolmo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=315#Scoreboard',
'2001 – Copenhague' => 'http://www.eurovision.tv/page/history/by-year/contest?event=266#Scoreboard',
'2002 -  Tallin' => 'http://www.eurovision.tv/page/history/by-year/contest?event=316#Scoreboard',
'2003 – Riga' => 'http://www.eurovision.tv/page/history/by-year/contest?event=217#Scoreboard',
'2004 - Estambul' => 'http://www.eurovision.tv/page/history/by-year/contest?event=8#Scoreboard',
'2005 – Kiev' => 'http://www.eurovision.tv/page/history/by-year/contest?event=159#Scoreboard',
'2006 – Atenas' => 'http://www.eurovision.tv/page/history/by-year/contest?event=334#Scoreboard',
'2007 – Helsinki' => 'http://www.eurovision.tv/page/history/by-year/contest?event=435#Scoreboard',
'2008 – Belgrado' => 'http://www.eurovision.tv/page/history/by-year/contest?event=1469#Scoreboard',
'2009 – Moscú' => 'http://www.eurovision.tv/page/history/by-year/contest?event=1482#Scoreboard',
'2010 – Oslo' => 'http://www.eurovision.tv/page/history/by-year/contest?event=1493#Scoreboard',
'2011 – Düsseldorf' => 'http://www.eurovision.tv/page/history/by-year/contest?event=1553#Scoreboard',
'2012 – Bakú' => 'http://www.eurovision.tv/page/history/by-year/contest?event=1593#Scoreboard',
'2013 – Malmö' => 'http://www.eurovision.tv/page/history/by-year/contest?event=1773#Scoreboard'];


$superdata = array();

foreach ($urls as $urlTitle => $url) {
  $html = file_get_html($url);

  $keys = array();
  
  foreach ($html->find('.votes_crosstable thead') as $value) {
    $img = $value->find('img');
    foreach($img as $valImg){
      array_push($keys, $valImg->alt);
    }
  }

  $data = array();

  foreach ($html->find('.votes_crosstable tbody tr') as $row) {
    $col = $row->find('td');
    $i = 0;

    $country = '';

    foreach($col as $val){
      if($val->class == 'participant'){
        $country = $val->find('span',0)->innertext;
        $data[$country] = array();
      } else {
        if(isset($keys[$i])){
          array_push($data[$country], array($keys[$i] => $val->innertext));
        }
        $i++;
      }
    }
  }

  $details = array();
  
  foreach ($html->find('.cb-EventInfo-facts .main-part') as $value) {
    $tits = $value->find('h3');
    $pes = $value->find('p');
    $idx = 0;
    foreach($tits as $tit) {
      //$details[$tit->innertext] = $pes[$idx];
      $category = $tit->innertext;
      $value = $pes[$idx]->innertext;

      if($category == 'Location') {
        $details[$tit->innertext] = $pes[$idx]->plaintext;
      } elseif ($category == 'Winner') {
        $details['linkWinner'] = 'http://www.eurovision.tv' . $pes[$idx]->find('a')[0]->href;
        $details['winner'] = $pes[$idx]->plaintext;
        $details['participantes'] = getCountrySongs($details['linkWinner']);
      } else {
        $details[$category] = $value;
      }
      $idx++;
    }
  }

  $superdata[$urlTitle] = array('data' => $data, 'details' => $details);

}

//print_r($superdata);

function getCountrySongs($link) {
  $html = file_get_html($link);

  $participantes = array();

  foreach ($html->find('.participant') as $row) {
    $participante = array(
      'pais' => $row->find('h3',0)->plaintext,
      'artista' => $row->find('h4',0)->plaintext,
      'cancion' => $row->find('h4 em',0)->plaintext,
      'posicion' => intval($row->find('.rank',0)->plaintext)
    );
    array_push($participantes, $participante);
  }

  return $participantes;
}


$fh = fopen("eurovi.json", 'w') or die("Error opening output file");
fwrite($fh, json_encode($superdata,JSON_UNESCAPED_UNICODE));
fclose($fh);