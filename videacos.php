<?php
setlocale(LC_ALL,"es_ES");
define('SITE_ROOT', dirname(__FILE__));
header('Content-Type: text/html; charset=utf-8');

require_once('./functions_csv.php');

$url = "./songs.csv";
$data = fileCSVToArray($url, 'titulo');
$data = $data[0];

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<?php foreach ($data as $temazo): ?>
  <?php if($temazo[0]['country'] == 'Spain'): ?>
    <div data-vid="<?php echo $temazo[0]['link'];?>" class="temazo" style="float: left; width: 480px; height: 360px; overflow: hidden;">
      <img style="width: 100%; height: auto; cursor: pointer;" src="<?php echo $temazo[0]['smallImg'];?>" alt="">
    </div>
  <?php endif; ?>
<?php endforeach; ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
  
  $('.temazo').click(function(){
    var $this = $(this);
    var vid = $this.data('vid');
    vid = vid.split('v=')[1];
    $this.html('<iframe width="480" height="360" src="//www.youtube.com/embed/'+vid+'" frameborder="0" allowfullscreen></iframe>')
  })
</script>
</body>
</html>