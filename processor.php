<?php
setlocale(LC_ALL,"es_ES");
define('SITE_ROOT', dirname(__FILE__));
header('Content-Type: text/html; charset=utf-8');

$url = "./eurovi.json";

ini_set('max_execution_time', 30000);

$json = file_get_contents($url);
$data = json_decode($json, TRUE);

//$nombres = 
$geo = fileCSVToArray('./datosGeopolits.csv', '');
$names = fileCSVToArray('./datosNames.csv', '');

//songstoJSON();

//print_r($data);die();

//print_r(getPosition($data, 'Spain', 2013)); die();

//print_r(getEverything($data));die();

//print_r(getRankins($data));die();
//writeJson('participa.json', getParticipacion($data));

//print_r(getDetails($data, 2012));
//print_r(getWinner($data, 2012));
//print_r(getSong($data, 'Spain', 2012));
//print_r(getSongs($data));

//print_r(puntuacionesXyear($data));
//print_r(getBestVoters($data));

/* todas las puntuaciones ordenadas puntosSort.csv
      -----------------------------------
$toExport = mejoresPuntuaciones($data);
$out = fopen('./puntosSort.csv', 'w');
foreach ($toExport as $key => $data) {
  fputcsv($out, $data);
};
fclose($out); 
die();*/ 

/* puntuaciones máximas en las votaciones de cada año puntos.csv
      -----------------------------------
$toExport = puntuacionesXyear($data);
simpleWriteArrayCSV('./puntos.csv', $toExport);*/ 

/* votaciones al ganador winner.csv
      -----------------------------------
$toExport = getBestVoters($data);
simpleWriteArrayCSV('./winner.csv', $toExport);*/ 

/* diferencia puntos primero/segundo victories.csv
      ----------------------------------- 
$toExport = getZeros($data);
writeArrayCSV('./ceros.csv', $toExport);*/

/* diferencia puntos primero/segundo victories.csv
      ----------------------------------- 
$toExport = getZeros2($data);
//writeArrayCSV('./ceros.csv', $toExport);
print_r($toExport);*/


//$toExport = getSeconds($data);
//$toExport = getParticipaCount($data);
//writeJson('partis.json', $toExport);
//print_r($toExport);



/* participantes y años years.csv
      ----------------------------------- 
$toExport = getTablaParticipa($data);
writeArrayCSV('./years.csv', $toExport);*/

/* participantes y años years.csv
      ----------------------------------- 
$toExport = getTablaPuestos($data);
//writeArrayCSV('./puestos.csv', $toExport);
print_r($toExport);*/

/* diferencia puntos primero/segundo diff.csv
      -----------------------------------
$toExport = getRatio12($data);
writeArrayCSV('./diff.csv', $toExport);*/

/* participantes de cada año para exportar a rankings.csv
      -----------------------------------
$toExport = getParticipacionCSV($data);
writeArrayCSV('./participa.csv', $toExport);*/

//print_r(getRankins($data));

/* rankings por años y exporta a rankings.csv
      -----------------------------------
$toExport = getRankingsCSV($data);
writeArrayCSV('./ranking.csv', $toExport);
*/

/*  media de votos a España paísa masVotadosSpain.csv
      -----------------------------------
$toExport = array(getMasVotadosSpain($data));
writeArrayCSV('./masVotadosSpain.csv', $toExport);*/

/* media de votos para cada paísa masVotados.csv
      -----------------------------------
$toExport = array(getMasVotados($data));
writeArrayCSV('./masVotados.csv', $toExport[0]);*/

//writeJson('votos.json', getMasVotados($data));
//writeJson('votosTotal.json', getMasVotadosTotal($data));
//writeJson('votos2.json', getMasVotadores($data));
//writeJson('votos2.json', getMasVotadores($data));
//print_R(getMasVotadores($data));

/* detalles de cada edicione
      ----------------------
$toExport = array(getAllDetails($data));
writeArrayCSV('./detalles.csv', $toExport[0]);*/

/* todas las canciones
      ---------------
$toExport = getSongs($data);
//print_r($toExport);die();
//writeArrayCSV('./songs.csv', $toExport[0]); 
writeJson('songs.json', getSongs($data));*/

/* posiciones de españa */
//writeJson('spanishPositions.json', getSpanishPositions($data));

function getSpanishPositions($data){

  $poss = array();

  foreach ($data as $year => $d2) {
    foreach ($d2['details']['participantes'] as $key => $val) {
      if($val['pais'] ==  'Spain  ') { $poss[intval($year)] = $val['posicion']; }
    }
  }
  return $poss;
}


/* procesa geopolítica */
writeJson('regions.json', processGeo($geo));

function processGeo($geo) {

  $givers = array();
  $takers = array();

  foreach ($geo as $key => $data) {

    if(!isset($givers[$data['País']])) $givers[$data['País']] = array();
    array_push($givers[$data['País']], $data);

    if(!isset($takers[$data['Vota países']])) $takers[$data['Vota países']] = array();
    array_push($takers[$data['Vota países']], $data);

  }

  $regions = array();

  foreach ($takers as $takerName => $taker) {
    foreach ($taker as $key => $data) {
      if(strlen($takerName) <= 1 && $data['País'] != 'Italia'){
        if($data['Geográficamente es país'] == $takerName) {
          if(!isset($regions[$takerName])) $regions[$takerName] = array();
          array_push($regions[$takerName], $data['País']);
        }
      }
    }
  }

  return array('givers'=>$givers, 'takers'=>$takers, 'regions'=>$regions);
}

/* arregla nombres de paises ingleses
      ---------------*/
//writeJson('songsES.json', arreglaCanciones());

function writeJson($fileName, $array) {
  $out = fopen($fileName, 'w');
  fwrite($out, json_encode($array));
  fclose($out);
}


function simpleWriteArrayCSV($fileName, $array) {
  $out = fopen($fileName, 'w');
  fputcsv($out, array_keys($array));
  fputcsv($out, $array);
  fclose($out);
}


function arreglaCanciones() {
  $songs = file_get_contents("./songs.json");
  $songs = json_decode($songs,true);

  $countries = array();
  $countriesdata = fopen('countries.csv','r');

  while ( ($countriescsv = fgetcsv($countriesdata) ) !== FALSE ) {
    $countries[$countriescsv[0]] = $countriescsv[1];
  }

  $countries = array_change_key_case($countries);

  foreach ($songs as $idx => $song) {
    if(isset($countries[strtolower($song['country'])])) { $songs[$idx]['country'] = $countries[strtolower($song['country'])]; };
  }

  return($songs);
}

function writeArrayCSV($fileName, $array) {
  $out = fopen($fileName, 'w');
  fputcsv($out, array_keys(reset($array)));
  foreach ($array as $row) {
    fputcsv($out, $row);
  }
  fclose($out);
}

function songstoJSON() {

  require_once './functions_csv.php';

  $url = "./songs.csv";
  $outp = "./songs.json";

  $csv = fileCSVToArray($url);

  writeJson($outp, $csv[0]);
}


function getPaises($data){

  $paises = array();

  foreach ($data as $evt => $datoEvt) {
    foreach ($datoEvt['data'] as $country => $votos) {
      if(!in_array($country, $paises)) array_push($paises, $country);
    }
  }
  
  return $paises;

}

function getBestVoters($data) {

  $rankin = getRankins ($data);
  $ptosYear = puntuacionesXyear($data);

  $countries = array();

  foreach ($data as $year => $dataYear) {

    $winner = $rankin[intval($year)];
    reset($winner);

    $winner = key($winner);
    $maxVotoYear = $ptosYear[intval($year)];

    foreach ($dataYear['data'] as $pais => $votos) {
      if($pais == $winner) {
        foreach ($votos as $key => $voto) {
          foreach ($voto as $count => $val) {
            if($val == $maxVotoYear) {
              if(!isset($countries[$count])) $countries[$count] = 0;
              $countries[$count]++;
            }
          }
        }
      }
    }
  }

  return $countries;
}

function puntuacionesXyear($data) {

  $ptos = array();
  $dat = $data;

  foreach ($dat as $evtName => $dataY) {

    $maxVoto = 0;

    foreach ($dataY['data'] as $country => $votos) {
      foreach ($votos as $count => $voto) {
        foreach ($voto as $cnt => $votoOK) {
          if($votoOK && $maxVoto < $votoOK) $maxVoto = $votoOK;
        }
      }
    }

    $ptos[intval($evtName)] = $maxVoto;
  }

  return $ptos;

}


/* archivo particionaciones final para ficha de país */

//$toExport = getParticipaCount($data);
//getCountrySpanish('Spain');
//writeJson('partis2.json', $toExport);
//print_r($toExport);

function getParticipaCount($data) {
  $data = getRankins ($data);

  $countries = array();

  foreach ($data as $year => $votes) {
    foreach ($votes as $pais => $voto) {

      if(!isset($countries[$pais])) {
        $countries[$pais] = array(
          'name' => getCountrySpanish($pais),
          'nameEN' => $pais, 
          'count' => 0, 
          'yrs' => array(),
          'bestPos' => getBest($data, $pais)
        );
      }

      $countries[$pais]['count'] = $countries[$pais]['count'] + 1;

      array_push($countries[$pais]['yrs'], $year );
    }
  }
  usort($countries, "ordenaByCount");
  
  //$countries = array_slice($countries, 0, 15);

  return $countries;

}

function ordenaByCount($b, $a) { return intVal($a['count']) > intVal($b['count']); }

function getCountrySpanish($name) {

  $names = $GLOBALS['names'];

  foreach ($names as $key => $value) {
    if($value['Country'] == $name) return $value['traduccion'];
  }

  echo 'no encontrado '.$name;
  return false;
}

function getBest($dataRankings, $name){

  $bestPos = 12310;
  $bestYear = 0;
  $bestPunt = 0;

  foreach ($dataRankings as $year => $data) {
    if(isset($data[$name])){

      $position = getPosition2($dataRankings, $name, $year);

      if($position <= $bestPos && $year > $bestYear) {
        $bestPos = $position;
        $bestYear = $year; 
        $bestPunt = $data[$name];
      }
    }
  }

  return array('bestPunt' => $bestPunt,'bestPos' => $bestPos, 'bestYear' => $bestYear);
}

function getParticipacion ($data) {
  $data = getRankins ($data);

  $countries = array();

  foreach ($data as $year => $votes) {
    foreach ($votes as $pais => $voto) {
      if(!isset($countries[$pais])) $countries[$pais] = array();
      array_push($countries[$pais], $year);
    }
  }

  return $countries;
}

function getParticipacionCSV($data) {

  $data = getParticipacion($data);

  $outp = array();

  for ($i = 1957; $i <= 2014; $i++) {
    $outp[$i] = array();
  }

  foreach ($data as $key => $value) {
    foreach ($value as $key2 => $value2) {
      for ($i = 1957; $i <= 2014; $i++) {
        $outp[$i][$key] = 'no';
        if(array_search($i, $value)) {
          $outp[$i][$key] = 'si';
        }
      }
    }
  }

  return $outp;
}

function getEverything ($data){

  $results = array();

  foreach ($data as $evt => $datoEvt) {
    $totals = array();
    foreach ($datoEvt['data'] as $country => $votos) {
      foreach ($votos as $voto => $pais) {
        foreach ($pais as $name => $val) {
          if(!isset($totals[$country])) $totals[$country] = 0;
          $totals[$country] = $totals[$country] + intval($val);
        }
      }
    }
    arsort($totals);
    $results[intval($evt)] = $totals;
  }
  
  return $results;

}

function getTablaPuestos($data) {

  $rankins = getRankins($data);
  $paises = getPaises($data);

  $outp = array();

    // CABECERA 
  $tablaHead = array('year');
  foreach ($paises as $pais) {
    array_push($tablaHead, $pais);
  }
  array_push($outp, $tablaHead);

  foreach ($rankins as $year => $ganadores) {

    $row = array($year);

    foreach ($paises as $pais) {
      $pos = getPosition2($rankins, $pais, $year);

      if($pos) {
        array_push($row, $pos);
      } else {
        array_push($row, '');
      }
    }
    
    array_push($outp, $row);

  }

  return $outp;
}

function getTablaParticipa($data) {

  $rankins = getRankins($data);
  $paises = getPaises($data);

  $outp = array();

    // CABECERA 
  $tablaHead = array('year');
  foreach ($paises as $pais) {
    array_push($tablaHead, $pais);
  }
  array_push($outp, $tablaHead);

  foreach ($rankins as $year => $ganadores) {

    $row = array($year);

    foreach ($paises as $pais) {
      if(isset($ganadores[$pais])) {
        array_push($row, $ganadores[$pais]);
      } else {
        array_push($row, '');
      }
    }
    array_push($outp, $row);
  }

  return $outp;
}

function getZeros2($data) {

  $rankins = getRankins($data);
  $outp = array(array('pais', 'ceros'));
  $paises = array();

  foreach ($rankins as $year => $ganadores) {
    
    foreach ($ganadores as $country => $puntos) {
      if($puntos == 0) {
        if(!isset($paises[$country])) {
          $paises[$country] = array($year);
        } else {
          array_push($paises[$country], $year);
        }

      }
    }
  }

  return $paises;
}

function getZeros($data) {

  $rankins = getRankins($data);
  $outp = array(array('pais', 'ceros'));
  $paises = array();

  foreach ($rankins as $year => $ganadores) {
    
    foreach ($ganadores as $country => $puntos) {
      if($puntos == 0) {
        if(!isset($paises[$country])) $paises[$country] = 0;
        $paises[$country]++;
      }
    }
  }

  foreach ($paises as $pais => $datos) {
    array_push($outp, array($pais, $datos));
  }

  return $outp;
}

function mejoresPuntuaciones($data) {

  $rankins = getRankins($data);

  $puntos = array();

  foreach ($rankins as $year => $rankin) {
    foreach ($rankin as $country => $pts) {
      array_push($puntos, array('count' => $country, 'pts' => $pts, 'year' => $year));
    }
  }

  usort($puntos, "ordenaRank");

  return $puntos;
}

function ordenaRank($a, $b) {
    if ($a['pts'] == $b['pts']) {
        return 0;
    }
    return ($a['pts'] > $b['pts']) ? -1 : 1;

}
//writeJson('victors.json', getVictories2($data));

function getVictories2($data) {

  $rankins = getRankins($data);

  $paises = array();

  foreach ($rankins as $year => $ganadores) {
    $posicion = 0;
    $ptos = 0;
    
    foreach ($ganadores as $country => $puntos) {
      $posicion++;

      if(!isset($paises[$country])) $paises[$country] = array();

      if ($posicion == 1) {
        $ptos = $puntos;
        array_push($paises[$country], $year);
      } elseif ($posicion == 2) {
        if($ptos == $puntos) {
          array_push($paises[$country], $year);
        }
      } elseif ($posicion == 3) {
        if($ptos == $puntos) {
          array_push($paises[$country], $year);
        }
      } elseif ($posicion == 4) {
        if($ptos == $puntos) {
          array_push($paises[$country], $year);
        }
      }
    }
  }

  return $paises;

}

function getVictories($data) {

  $rankins = getRankins($data);
  $outp = array(array('pais', 'primero', 'segundo', 'tercero'));

  $paises = array();

  foreach ($rankins as $year => $ganadores) {
    $posicion = 0;
    $ptos = 0;
    
    foreach ($ganadores as $country => $puntos) {
      $posicion++;

      if(!isset($paises[$country])) $paises[$country] = array('prim' => 0, 'sec' => 0, 'tri' => 0);

      if ($posicion == 1) {
        $ptos = $puntos;
        $paises[$country]['prim'] += 1;
      } elseif ($posicion == 2) {
        if($ptos == $puntos) {
          $posicion -= 1;
          $paises[$country]['prim'] += 1;
        } else {
          $paises[$country]['sec'] += 1;
        }
      }elseif ($posicion == 3) {
        $paises[$country]['tri'] += 1;
      }
    }
  }

  foreach ($paises as $pais => $datos) {
    array_push($outp, array($pais, $datos['prim'], $datos['sec'], $datos['tri']));
  }

  return $outp;
}


function getSeconds($data) {
  $rankins = getRankins($data);

  $outp = array();

  foreach ($rankins as $year => $ganadores) {

    $best = 0;
    $bestStr = '';
    $second = 0;
    $secondStr = '';
    $idx = 0;

    foreach ($ganadores as $country => $puntos) {
      if($idx == 0) {
        $bestStr .= $country;
        $best = $puntos;
      } else {
        if($best == $puntos) { 
          $bestStr .= ', '.$country;
        } else {
          if($second == 0) {
            $second = $puntos;
            $secondStr = $country;
          } elseif($second == $puntos) {
            $secondStr .= ', '.$country;
          }
        }
      }
      $idx ++;
    }

    $row = array($year, $bestStr, $best, $secondStr, $second, intval(100*($best - $second)/$best));
    array_push($outp, $row);

  }

  $seconds = array();

  foreach ($outp as $yr => $datosYear) {
    $ifx2++;
    $multiple = split(', ', $datosYear[3]);
    foreach ($multiple as $part) {
      if(!isset($seconds[$part])) {
        $seconds[$part] = array($datosYear[0]);
      } else {
        array_push($seconds[$part], $datosYear[0]);
      }
    }
    
  }

  print_r($seconds);die();

  return $outp;
}


function getRatio12($data) {
  $rankins = getRankins($data);

  $outp = array(array('year','first', 'points first', 'second', 'points second', 'diff'));
  
  foreach ($rankins as $year => $ganadores) {

    $best = 0;
    $bestStr = '';
    $second = 0;
    $secondStr = '';
    $idx = 0;

    foreach ($ganadores as $country => $puntos) {
      if($idx == 0) {
        $bestStr .= $country;
        $best = $puntos;
      } else {
        if($best == $puntos) { 
          $bestStr .= ', '.$country;
        } else {
          if($second == 0) {
            $second = $puntos;
            $secondStr = $country;
          } elseif($second == $puntos) {
            $secondStr .= ', '.$country;
          }
        }
      }
      $idx ++;
    }

    $row = array($year, $bestStr, $best, $secondStr, $second, intval(100*($best - $second)/$best));
    array_push($outp, $row);
  }

  return $outp;
}

function getRankins ($data) {

  $results = array();

  foreach ($data as $evt => $datoEvt) {
    $totals = array();
    foreach ($datoEvt['data'] as $country => $votos) {
      foreach ($votos as $voto => $pais) {
        foreach ($pais as $name => $val) {
          if(!isset($totals[$country])) $totals[$country] = 0;
          $totals[$country] = $totals[$country] + intval($val);
        }
      }
    }
    arsort($totals);
    $results[intval($evt)] = $totals;
  }
  
  return $results;
}

function getRankingsCSV($data) {
  $rank = getRankins ($data);

  $outp = array();
  foreach ($rank as $year => $votos) {
    $outp[$year] = array();
    foreach ($votos as $country => $totalVotos) {
      array_push($outp[$year], $country.' ( '.$totalVotos.' votos)');
    }
  }
  return $outp;
}

function getMasVotadores($data) {

  $outp = array();
  $count = array();

  foreach ($data as $year => $data) {
    foreach ($data['data'] as $votado => $votos) {
      foreach ($votos as $voto) { 
        foreach ($voto as $votador => $valor) { 
          if(!isset($outp[$votador])) $outp[$votador] = array();
          if(!isset($outp[$votador][$votado])) $outp[$votador][$votado] = 0;
          $outp[$votador][$votado] += $valor;
          if(!isset($count[$votador])) $count[$votador] = array();
          if(!isset($count[$votador][$votado])) $count[$votador][$votado] = 0;
          $count[$votador][$votado] ++;
        }
      }
    }
  }

  foreach ($outp as $country => $data) {
    foreach ($data as $target => $val) {
      $outp[$country][$target] = $outp[$country][$target] / $count[$country][$target];
    }
  }

  return($outp);
}

function getMasVotadoresTotal($data) {

  $outp = array();

  foreach ($data as $year => $data) {
    foreach ($data['data'] as $votado => $votos) {
      foreach ($votos as $voto) { 
        foreach ($voto as $votador => $valor) { 
          if(!isset($outp[$votador])) $outp[$votador] = array();
          if(!isset($outp[$votador][$votado])) $outp[$votador][$votado] = 0;
          $outp[$votador][$votado] += $valor;
        }
      }
    }
  }

  return($outp);
}

function getMasVotadosTotal($data) {

  $outp = array();

  foreach ($data as $year => $data) {
    foreach ($data['data'] as $votado => $votos) {
      foreach ($votos as $voto) { 
        foreach ($voto as $votador => $valor) { 
          if(!isset($outp[$votado])) $outp[$votado] = array();
          if(!isset($outp[$votado][$votador])) $outp[$votado][$votador] = 0;
          $outp[$votado][$votador] += $valor;
        }
      }
    }
  }

  return($outp);
}

function getMasVotados($data) {

  $pais = array();

  foreach ($data as $key => $value) {
    foreach ($value['data'] as $key2 => $value2) {

      if(!isset($pais[$key2])) $pais[$key2] = array();

      foreach ($value2 as $votos => $pais2) {
        foreach ($pais2 as $key3 => $voto) {
          if(!isset($pais[$key2][$key3])) {
            $pais[$key2][$key3] = array('totals' => $voto, 'votos' => array());
          } else {
            $pais[$key2][$key3]['totals'] = $pais[$key2][$key3]['totals']+ $voto;
          }
          array_push($pais[$key2][$key3]['votos'], array($key => $voto));
        }
      }
    }
  }

  $paisMin = array();

  foreach ($pais as $key => $value) {
    foreach ($value as $country => $datos) {
      if(!isset($paisMin[$key])) $paisMin[$key] = array();
      if($key != $country) {
        $paisMin[$key][$country] = $datos['totals'] / count($datos['votos']);
      }
    }
    arsort($paisMin[$key]);
  }

  return $paisMin;
}

function getMasVotadosSpain($data) {
  $data = getMasVotados($data);

  foreach ($data as $key => $value) {
    if($key == 'Spain') return $value;
  }

  return false;
}

function getDetails($data, $year) {
  foreach ($data as $key => $value) {
    if(intval($key) == $year){
      $outp = $value['details'];
      unset($outp['participantes']);
      return $outp;
    }
  }
}

function getAllDetails($data) {

  $outp = array();

  foreach ($data as $key => $value) {
    $outp[intval($key)] = $value['details'];
    unset($outp[intval($key)]['participantes']);
  }

  return $outp;
}

function getWinner($data, $year) {

  $ganadores = array();

  foreach ($data as $key => $value) {
    if(intval($key) == $year){
      foreach ($value['details']['participantes'] as $key2 => $value2) {
        if($value2['posicion'] == 1) array_push($ganadores, $value2);
      }
    }
  }
  return $ganadores;
}

function getSongs($data) {
  $songs = array();

  foreach ($data as $key => $value) {
    foreach ($value['details']['participantes'] as $key2 => $value2) {

      //$dataVideo = queryVideo($busqueda);
      $artista = substr($value2['artista'], 0, strlen($value2['artista']) - strlen($value2['cancion']));
      if(strpos($artista, ' - ') + 3 == strlen($artista)) $artista = substr($artista, 0, -3);

      $youtube = queryYoutube(html_entity_decode($artista, ENT_QUOTES).' '.html_entity_decode($value2['cancion'], ENT_QUOTES).' '.
        html_entity_decode($value2['pais'], ENT_QUOTES).' '.intval($key).'- Eurovision');

      array_push($songs, array(
        'year'=>intval($key), 
        'country'=>html_entity_decode($value2['pais'], ENT_QUOTES), 
        'artista'=>html_entity_decode($artista, ENT_QUOTES), 
        'titulo'=>html_entity_decode($value2['cancion'], ENT_QUOTES), 
        'posicion' => html_entity_decode($value2['posicion'], ENT_QUOTES), 
        'smallImg' => $youtube['smallImg'],
        'bigImg' => $youtube['bigImg'],
        'link' => $youtube['link']
      ));
    }
  }
  return $songs;
}

function getPosition2($ranking, $countr, $year) {

  $prevPts = 0;
  $position = 0;

  foreach($ranking[$year] as $count => $pts) {

    if ($prevPts != $pts) $position++;
    $prevPts = $pts;
    if($countr == $count) return $position;

  }

  return false;
}

function getPosition($data, $countr, $year) {

  $ranking = getRankins($data);

  $prevPts = 0;
  $position = 0;

  foreach($ranking[$year] as $count => $pts) {

    if ($prevPts != $pts) $position++;
    $prevPts = $pts;
    if($countr == $count) return $position;

  }

  return false;
}

function getSong($data, $countr, $year) {
  foreach ($data as $key => $value) {
    if(intval($key) == $year){
      foreach ($value['details']['participantes'] as $key2 => $value2) {
        if(trim($value2['pais']) == $countr) return $value2;
      }
    }
  }
  return false;
}

function processYoutube($results){
  if(isset($results[0]) && isset($results[0]['modelData']['id']['videoId'])) {
    return array(
      'link' => 'https://www.youtube.com/watch?v='.$results[0]['modelData']['id']['videoId'],
      'smallImg' => $results[0]['modelData']['snippet']['thumbnails']['high']['url'],
      'bigImg' => $results[0]['modelData']['snippet']['thumbnails']['default']['url']
    );
  } else {

    return array(
      'link' => '',
      'smallImg' => '',
      'bigImg' => ''
    );
  }
}

function queryYoutube($search){
  try{
    set_include_path('./google-api-php-client-master/src');
    
    // Call set_include_path() as needed to point to your client library.
    require_once 'Google/Client.php';
    require_once 'Google/Service/YouTube.php';

    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

    $DEVELOPER_KEY  = 'AIzaSyB4J7IBYD0FnBC6oV4qZikOZxdKYn-vyrI';

    $client = new Google_Client();
    $client->setDeveloperKey($DEVELOPER_KEY);

    $youtube = new Google_Service_YouTube($client);

    // Call the search.list method to retrieve results matching the specified
    // query term.
    $searchResponse = $youtube->search->listSearch('id,snippet', array(
      'q' => $search,
      'maxResults' => 5,
    ));

    return processYoutube($searchResponse['items']);

  } catch(Exception $e) {
    return array(
      'link' => '',
      'smallImg' => '',
      'bigImg' => ''
    );
  }
}

function fileCSVToArray ($dataFilePath, $column='') {

    // Variables
    $firstLine = true;
    $charsCat = array();

    $index = 0;

    // Abrimos el fichero
    $file = fopen($dataFilePath,"r");

        if ( $file===false ) {
      // error reading or opening file
           return true;
        }

    // Lo recorremos
    while (($arr = fgetcsv($file, 4000, ",")) !== false) {
      if ($firstLine == true) {
        $firstLine = false;
        $cols = $arr;
      } else {

        $newRow = array();
        for ($i=0; $i < count($cols); $i++) { 
          $newRow[$cols[$i]] = trim($arr[$i]);
        }

        if(isset($column) && $column!=''){
          $charsCat[$newRow[$columm]] = $newRow;
        } else {
          $charsCat[$index] = $newRow;
          $index++;
        }
      }
    }





    return $charsCat;
  } 

